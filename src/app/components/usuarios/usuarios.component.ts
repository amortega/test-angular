import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  usuarios: Array<any> = [];
  urlRequest: String;
  loading: boolean = true;

  constructor(
    private usuarioService: UsuarioService
  ) { }

  ngOnInit(): void {
    this.obtener_usuarios();
  }

  obtener_usuarios(): void {
    this.usuarioService.getUsuarios()
      .then( (usuarios: Array<any>) => {
        this.usuarios = usuarios;
        this.loading = false;
      })
      .catch(error => {
        console.error(error);
        this.loading = false;
      });
  }

}
