import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  urlRequest: string = 'https://jsonplaceholder.typicode.com/users';
  
  constructor() { }

  getUrlRequest(): string {
    return this.urlRequest;
  }
}
