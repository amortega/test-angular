import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(
    private http: HttpClient,
    private config: ConfigService
  ) { }

  getUsuarios() {
    return this.http.get(this.config.getUrlRequest()).toPromise();
  }
}
